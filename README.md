# Quick Start

```bash
$ git clone git@gitlab.com:st0502123/jwt-node-auth.git
$ cd jwt-node-auth
$ npm install
$ npm run dev
```

# Api

*<root_api>* = localhost:3000/api/v1

| Method | Url | body (x-www-form-urlencoded) | Note |
| -------|-----|------------------------------|----- |
| POST | *<root_api>*/users | name, password | 新增一個帳戶 |
| POST | *<root_api>*/login | name, password | 登入帳戶 |



