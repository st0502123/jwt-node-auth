const bcrypt = require('bcrypt');
const User = require('../models/users');

const connUri = process.env.MONGO_LOCAL_CONN_URL;

module.exports = {
	add: (req, res) => {
		let result = {};
		let status = 201;

 		const {name, password} = req.body;
		const user = new User({name, password});

		user.save((err, user)=>{
				
			if (err) {
				status = 500;
				result.status = status;
				result.error = err;
			} else {
				result.status = status;
				result.result = user;
			}

			res.status(status).send(result);
		});

	},

	login: (req, res)=>{
		const {name, password} = req.body;

		let result = {};
		let status = 200;
			
		User.findOne({name}, (err, user)=>{
			if (!err && user) {

				bcrypt.compare(password, user.password).then(match=>{
					if (match) {
						result.status = status;
						result.result = user;
					} else {
						status = 401;
						result.status = status;
						result.error = 'Authentication error';
					}
					res.status(status).send(result);
				}).catch (err=>{
					status = 500;
					result.status = status;
					result.error = err;
					res.status(status).send(result);
				});

			} else {
				status = 404;
				result.status = status;
				result.error = err;
				res.status(status).send(result);
			}
		});
	},
}
