require('dotenv').config();

const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const connUri = process.env.MONGO_LOCAL_CONN_URL;

mongoose.Promise = global.Promise;

const app = express();
const router = express.Router();

const environment = process.env.NODE_ENV;
const stage = require('./config')[environment];

const routes = require('./routes/index');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true,
}));

if (environment !== 'production'){
	app.use(logger('dev'));
}

app.use('/api/v1', (req, res, next)=>{
	console.log("Something happened");
	next();
});


app.use('/api/v1', routes(router));


mongoose.connect(connUri, {
	useNewUrlParser: true
}).then(() => {
	console.log('Successfully connected to the database');
	app.listen(stage.port, ()=>{
		console.log(`Server now listening at localhost:${stage.port}`);
	});
}).catch(err => {
	console.log('Could not connect to the database. Exiting now ... ', err);
	logger.info('Database connection unavailable, not running app');
	process.exit();
});


